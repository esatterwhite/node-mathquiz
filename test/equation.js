var assert = require( 'assert' );
var equation = require("../lib/equation");

describe('equation', function(){
	describe("~Equation Class", function(){
		it("should solve for x", function(){
			var e = new equation.Equation( "x / 10", 5);
			assert.equal(e.correct(50), true)
		});

		it("Should solve for x & y",function(){
			var e = new equation.Equation('( x + y ) /2', 10,{args:"x,y"});
			assert.equal( e.correct(10,10), true );
			assert.equal( e.correct(30,-10), true );
			assert.notEqual( e.correct(100, 12), true)
		})

		it("should match solved unsolved questions", function(){
			var e = new equation.Equation( "2 + 2", 4);
			assert.equal(e.correct(4), true)
		});

		it("should not match solved unsolved questions with incorrect answers", function(){
			var e = new equation.Equation( "2 + 2");
			assert.equal(e.correct(7), false)
		});

		it("Should solve for a", function( ){
			var e = new equation.Equation("a + 10", 11,{args:"a"});
			assert(e.correct(1));
		})

		describe("#toString", function(){
			it("should convert Math.pow to ^",function(){
				var equation_str = "1 + Math.pow( 2,5 )"
				var converted_str = "1 + 2 \u2227 5 = x";
				var e = new equation.Equation(equation_str)
				assert.equal( e.toString(), converted_str );
			});
			
			it("should convert Math.sin to ∿",function(){
				var equation_str = "1 + Math.sin(-1)"
				var converted_str = "1 + \u223F -1 = x";
				var e = new equation.Equation(equation_str)
				assert.equal( e.toString(), converted_str );
			});

			it("should convert Math.abs to |x|",function(){
				var equation_str = "1 + Math.abs(-1)"
				var converted_str = "1 + \u2223-1\u2223 = x";
				var e = new equation.Equation(equation_str)
				assert.equal( e.toString(), converted_str );
			});

			it("should convert Math.abs to √",function(){
				var equation_str = "1 + Math.sqrt(10)"
				var converted_str = "1 + \u221A 10 = x";
				var e = new equation.Equation(equation_str)
				assert.equal( e.toString(), converted_str );
			});
		})

		describe("#correct", function(){
			it("return false for incorrect answers", function(){
				var e = new equation.Equation("2 + 2");
				assert.strictEqual( e.correct( 5 ), false)
			});

			it("return true for correct answers", function(){
				var e = new equation.Equation("x + 2", 5)
				assert.strictEqual(e.correct(3), true);
			});
		})

		describe(".solved", function(){
			it("Should not be solved when an answer is not given", function(){
				var e = new equation.Equation('x + 1', 40)
				assert( !e.solved )
			})

			it("Should be solved when an answer is given", function(){
				var e = new equation.Equation("( Math.pow(2,4) + 10 ) / Math.PI")
				assert( e.solved )
			})
		});
	})

	describe("#from",function( ){
		it('Should convert strings to functions', function(){
			var fn = equation.from("2");
			assert(typeof fn === 'function')
		})

		it("should accept x as a default param", function( ){
			var fn = equation.from("x * 2")
			assert( typeof fn === "function")

			assert.strictEqual( 8, fn(4) );
		})

		it("should allow for custom param names", function( ){
			var fn = equation.from("a * 2", "a")
			assert( typeof fn === "function")

			assert.strictEqual( 8, fn(4) );
		})

	})
});
