var os = require('os');

exports.redishost  = "localhost";
exports.redisport  = 6379;
exports.serverport = 3000;
exports.workers    = os.cpus().length
exports.logdir     = "."
exports.verbose      = false


exports.STATIC_PATH = "/static"
exports.BOWER_PATH  = "/bower"