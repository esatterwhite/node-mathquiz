var nconf = require( 'nconf' )
  , defaults = require( './defaults' );

module.exports = nconf
	  .argv()
	  .env()
	  .file( 'config.json' )
	  .defaults( defaults )
