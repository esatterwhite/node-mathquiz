exports.sqrt = "\u221A %s"
exports.pow  = "%s \u2227 %s"
exports.sin  = "\u223F %s"
exports.abs  = "\u2223%s\u2223"

// Math\.(sqrt|pow|sin|abs) - math operations that require function call
// \(\s*(\d+\.?\d*)  - opening paraen, any amount of space and a digit w/ optional decimals
// s*(?:\,)?  - Any ammount of space, optional comman, not captured 
// (\s*\d+\.?\d*)?\s*\) // optionally, a second decimal capture

exports.MATH_REGEX = /Math\.(sqrt|pow|sin|abs)\(\s*((?:\+|\-)?\d+\.?\d*)\s*(?:\,)?(\s*(?:\+|\-)?\d+\.?\d*)?\s*\)/g
exports.EMPTY_STR = "";
exports.EMPTY_ARR = new Array(0);
exports.EMPTY_OBJ = {};
exports.NOOP = new Function;

exports.events = {
	CORRECT:"correct"
	,INCORRECT:"incorrect"
}