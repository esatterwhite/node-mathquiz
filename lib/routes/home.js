var fs = require("fs")
var path = require("path")
console.log(__dirname)
var packagedata = JSON.parse(
	 fs.readFileSync(
	 	path.join(
	 		__dirname, "..","..", 'package.json'
 		) 
 	)
 )
module.exports = function( req, res, next ){
	res.render("chat",packagedata )
}