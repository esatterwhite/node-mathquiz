 /*jshint laxcomma:true, smarttabs: true */
'use strict'

/**
 * Module for converting text into math equations to be validated
 * @author Eric Satterwhite
 * @requires util
 * @requires events
 * @requires lib/exceptions
 * @requires lib/constants
 */

var util = require( 'util' )
  , events = require( 'events' )
  , exceptions = require("./exceptions")
  , constants = require('./constants')

/**
 * creates an expresion procedure using the supplied equation body
 * @param {String} body The body of the function to execute, sans return statement
 * @param {String} varaible The name of the variable contain with in the equation body
 * @return equation The final euqation to use in the graghing equation
 **/
exports.from = function( text, variable ){
	"use strict"; 

	var fn       // the function that will be returned
	  , is_valid // a boolean value if the equation returns a valid response
	  , result
	  , vars = []
	  ;

	text     = util.format( "return %s", text );

	variable = ( variable || "x" ).split(",").map(function(v){
		vars.push(1)
		return v.trim()
	});

	fn       = new Function( variable, text );

	result   = fn.apply( null, vars )
	is_valid = typeof result === 'number' && !isNaN( result );

	if( is_valid ){
		return fn;
	}
		
	throw exceptions.EquationError;
};

/**
 * DESCRIPTION
 * @class module:NAME.Thing
 * @param {TYPE} NAME DESCRIPTION
 * @example var x = new NAME.Thing({});
 */
function Equation( body, result, options )/** @lends module:NAME.Thing.prototype */{
	options      = options || {};
	this._body   = body
	this._eq     = exports.from( body, options.args );
	this._answer = result ;
	this._args   = options.args
	events.EventEmitter.call(this);
};

// can do event-y things
util.inherits( Equation, events.EventEmitter )


/**
 * DESCRIPTION
 * @method module:lib/equation.Equation#toString
 * @return
 **/
Equation.prototype.toString = function(){
	return util.format( "%s = %s", this.convert( this._body ).trim(), this.answer ).trim()
}

/**
 * DESCRIPTION
 * @method module:lib/equation.Equation#convert
 * @param {String} content 
 * @return
 **/
Equation.prototype.convert = function( content ){
	return content.replace(constants.MATH_REGEX,function( $1, $2, $3, $4, $5, $6 ){
		return util.format( constants[$2], $3, ( $4 || constants.EMPTY_STR ) );
	});
}


// Dynamic properties
Object.defineProperties(Equation.prototype,{

	answer:{
		/**
		 * DESCRIPTION
		 * @name Equation#get:answer
		 * @method module:lib/equation.Equation.answer
		 * @return {String|Number}
		 **/
		get: function(){
			return this._answer == null ? this.variables : this._answer;
		}

		/**
		 * DESCRIPTION
		 * @name Equation#set:answer
		 * @method module:lib/equation.Equation.answer
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return
		 **/
		,set:function( value ){
			this._answer = typeof value == "number" ? parseFloat( value ) : null;
			return this;
		}
	}

	/**
	 * DESCRIPTION
	 * @method module:lib/equation.Equation
	 * @param {TYPE} NAME ...
	 * @fires module:lib/equation.Equation#correct
	 * @fires module:lib/equation.Equation#incorrect
	 * @return Boolean
	 **/
	,correct: {
		value: function( guess ){
			var success;
			if( this.solved ){
				// if their is nothing to solve for( solved and no answer)
				// we are looking for the answer

				// 2 + 2 is solved - the answer is the guess
				success = this._eq(guess) == (this._answer || guess);
			} else{
				success = this._answer == this._eq.apply( null, arguments );
			}

			this.emit(
				success ? constants.events.CORRECT : constants.events.INCORRECT
			  , guess
			);
			return success;
		}
	}

	,solved: {
		/**
		 * DESCRIPTION
		 * @method module:lib/equation.Equation
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return
		 **/
		get: function( ){
			return this._answer == null;
		}
	}

	,variables:{
		/**
		 * DESCRIPTION
		 * @method module:lib/equation.Equation
		 * @param {TYPE} NAME ...
		 * @param {TYPE} NAME ...
		 * @return
		 **/
		get: function( ){
			return this._args == null ? "x": Array.isArray( this._args) ? this._args.join(', ') : this._args;
		}
	}
});


exports.Equation = Equation;