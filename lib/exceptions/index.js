var equation = require('./equation')
exports.EquationError = new equation.EquationError("equations must return a numeric value")