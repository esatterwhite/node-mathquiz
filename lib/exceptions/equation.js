/*jshint laxcomma:true, smarttabs: true */
/**
 * DESCRIPTION
 * @module exceptions/equation
 * @author Eric Satterwhite
 * @requires util
 **/

var util = require('util')

/**
 * DESCRIPTION
 * @class module:lib/exceptions.EquationError
 * @param {String} msg Exception error message
 * @lends module:lib/exception.EquationError.prototype
 */
function EquationError( msg ){
	this.name = 'EquationError';
	this.message = msg
	Error.call( this );
}

util.inherits( EquationError, Error);
exports.EquationError = EquationError;
