var axon    = require('axon')
  , conf    = require('../../conf')
  , events  = require('events')
  , logger  = require("../logging")
  , pull

var sub_port = parseInt( conf.get('serverport'), 10) + 3
var pull_port = parseInt( conf.get('serverport'), 10) + 2 
var req_port = parseInt( conf.get('serverport'), 10) + 1 

exports.req = axon.socket( 'req')
exports.sub = axon.socket('sub-emitter')
pull         = axon.socket( 'pull' )


// connection to hub ports
pull.connect( pull_port )
exports.pull = new events.EventEmitter();
pull.on('message', function(){
	logger.debug("emitting pull events", arguments)
	exports.pull.emit.apply( exports.pull, arguments )
})
logger.info('pull socket connected to %s' , pull_port )

exports.req.connect( req_port )
logger.info( "Req socket connected to %s ", req_port )

exports.sub.connect( sub_port )
logger.info( "sub socket connected to %s ", req_port )
