var redis
	,cli
	,active_question
	,questions
	,Equation
	,logger;

Equation = require('../equation').Equation;
questions = require('../data/questions')
logger = require('../logging')
redis = require('redis')
cli = redis.createClient()
active_question = ""

function random( arr ){
	var rand = Math.floor( Math.random() * ( arr.length - 1 )  );
	return ( arr.length ) ? arr[ rand ] : null;
}

exports.answer = function( payload, callback ){
	cli.hgetall("quiz:active", function( err, result ){
		result = result || exports.next();
		logger.debug("question loogup", arguments )

		if( err ){
			return callback( false )
		}

		return callback( 
				new Equation( result.question, result.answer, result.args )
					.correct( payload.answer )
			);
	})

	
}

exports.next = function( ){
	var transaction = cli.multi();
	active_question = (+ new Date() ).toString( 12 )
	var n = random( questions );
	transaction
		.hmset( "quiz:active", "id", active_question, "question", n[0])

	if( n[1] ){
		transaction.hmset('quiz:active', 'answer', n[1] )
	}
	if( n[2] ){
		transaction.hmset('quiz:active', 'args', n[2] )
	}

	transaction.exec(redis.print);
	return new Equation( n[0], n[1],n[2] )
}