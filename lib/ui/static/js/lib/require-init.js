;(function(global){
	STATIC_PATH = "/static"
	BOWER_PATH  = "/bower"
	global.belly = require({
		context:"bellyv1"
		,baseUrl:"/static/js/modules"
		,shim:{
			mootools:{
				exports:"MooTools"
			}
			,modernizr:{
				exports:"Modernizr"
			}
			,primus:{
				exports:"Primus"
			}
		}
		,paths:{
			modernizr:"/bower/modernizr/modernizr"
			,mootools:"/static/js/lib/mootools"
			,primus:"/primus/primus"
		}
	})
}(this));