'use strict';
var express = require( 'express' )
  , Primus  = require( 'primus' )
  , path    = require( 'path' )
  , redis   = require( 'redis' )
  , rooms   = require( 'primus-redis-rooms' )
  , http    = require( 'http' )
  , conf    = require( '../conf' )
  , swig    = require( 'swig' )
  , logger  = require( './logging' )
  , routes  = require( './routes')
  , app     = express()
  , handlers = require('./handlers')
  , connections = require('./hub/connections')
  , primus
  , server

server = http.createServer( app );
primus = new Primus(server,{
		transformer: 'websockets'
		,redis:{
				host:conf.get( 'redishost' )
			, port:conf.get( 'redisport' )
		}
});

primus.use( 'redis', rooms )

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/ui/views');

app.configure(function(){
  app.use( express.bodyParser() )
  app.use(
      conf.get("STATIC_PATH")
    , express.static( 
      path.normalize( 
        path.join(
          __dirname, "ui", "static" 
        ) 
      ) 
    ) 
  )
  app.use(
      conf.get("BOWER_PATH")
    , express.static( 
      path.normalize( 
        path.join(
          __dirname, "ui", "theme", "bower_components" 
        ) 
      ) 
    ) 
  )


  // use inception to log http request
  app.use( express.logger( logger.express ) )
  app.set('view cache', false);
})

app.get('/', routes.home )

primus.on('connection',handlers.server.connection )

server.listen( conf.get( 'serverport' ), "0.0.0.0" )
connections.pull.on('next', function( equation ){
  logger.info("got next event", arguments )
  primus.room('belly::quiz').write( {evt:"next", "data": equation})
})