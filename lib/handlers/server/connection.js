"use strict";
/**
 * DESCRIPTION
 * @module NAME
 * @author Eric Satterwhite
 * @requires util
 **/
var logger = require('../../logging')
  , conf   = require('../../../conf')
  , connections = require('../../hub/connections')
  , empty_object = {}

function onData( packet ){
	logger.debug("socket got data")
	logger.info( packet )

	this.write('test', {type:"event", data:{test:1} })
	logger.debug("sending answer")
	
	connections.req.send(
		packet.event, packet || empty_object
	  , function( data ){
			this.write({"evt":"answer:reply", data:data})
		}.bind( this )
	)
}


module.exports = function( socket ){
	logger.warning("connection")
	socket.on('data', onData.bind( socket ) )
	socket.join("belly::quiz")
};
