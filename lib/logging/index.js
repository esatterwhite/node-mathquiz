/**
 * Common logging interfae
 * @module logging
 * @author Eric Satterwhite
 * @requires winston
 * @requires path
 * @requires conf
 **/

var winston = require( 'winston' )
  , path    = require( 'path' )
  , conf    = require('../../conf')
  , log_dir
  , cli
  , loggers
  , default_log
  , error_log
  , exceptionloggers
  , DEBUG;


log_dir     = conf.get( 'logdir' );
DEBUG       = !!conf.get( 'verbose' );
default_log = path.join( log_dir, 'quiz.log' );
error_log   = path.join( log_dir, 'quiz.error.log');

exceptionloggers = [
		new winston.transports.DailyRotateFile({ 
			filename: error_log, 
			prettyPrint:true, 
			label:'errors' 
		})
];

loggers = [
	new winston.transports.DailyRotateFile({
		label:"file"
		, filename: default_log
		, prettyPrint:false
		, level:"emerg"
		, json: false
		, options:{
			highWatermark:24
			,flags:'a'
		}
	})
]


// if debug mode include a stdout logger
if( DEBUG ){
	cli = new winston.transports.Console({
		label:"quiz " + process.pid
		, prettyPrint:true
		, colorize:true
		, exitOnError:false
		, timestamp:true
		, level:"emerg" 
	});

	loggers.push( cli );
	exceptionloggers.push( cli );
}


// expose an instance
module.exports = new (winston.Logger)({
	transports: loggers
	,exceptionHanders: exceptionloggers
	,levels: winston.config.syslog.levels
	,colors: winston.config.syslog.colors
	,padLevels: true
})

// expose error hooks
module.exports.exception = winston.exception
module.exports.express = {
	stream:{
		write: function(message, encoding){
			module.exports.info(message)
		}
	}
	,format: ':remote-addr - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ( :response-time ms )'
}