var axon
  , redis
  , server
  , push
  , pub
  , conf
  , logger
  , Equation

axon   = require("axon");
redis  = require('redis');
server = axon.socket('rep');
push    = axon.socket('push');
pub    = axon.socket('pub-emitter');
conf   = require("../conf");
logger = require("./logging")
Equation = require('./equation').Equation;
commands = require("./hub/commands")


server.bind( parseInt( conf.get('serverport'), 10) + 1 );
push.bind( parseInt( conf.get('serverport'), 10) + 2 );
pub.bind( parseInt( conf.get('serverport'), 10) + 3 );


server.on('message', function( action, payload, reply ){
	if( typeof commands[ action ] !== 'function'){
		logger.info( typeof commands[action])
		logger.warning("unknown command %s", action )
		logger.info("know commans: %s", Object.keys(commands ).join(', '))
		return;
	}

	logger.debug("executing %s command", action )
	var ans = ( Math.random() + Math.random() ) * 100
	commands[action]( payload, function repCallback (ans){
		logger.info('correct: %s', ans )
		reply( ans )
		pub.emit('question:answered', ans )
		push.send('next', commands.next().toString() )
	})
})


